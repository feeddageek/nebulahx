package macros;

import haxe.macro.Compiler;
import haxe.macro.Context;
import haxe.macro.Expr;
import sys.FileSystem;

class Macros {
	public static macro function getDefined(name:String):ExprOf<String> {
		return macro $v{Context.definedValue(name)};
	}

	#if macro
	public static function gitDefine() {
		function getCommandOutput(command:String, args:Array<String>) {
			var process = new sys.io.Process(command, args);
			if (process.exitCode() != 0) {
				var message = process.stderr.readAll().toString();
				var pos = Context.currentPos();
				Context.error('Cannot execute `$command` ${args.join(' ')}. ' + message, pos);
			}

			return process.stdout.readLine();
		}

		var gitDescribe:String = getCommandOutput('git', ['describe', '--long', '--always', '--abbrev=8']);
		var gitHead:String = getCommandOutput('git', ['rev-parse', '--abbrev-ref', 'HEAD']);

		// Generates a string expression
		Compiler.define('gitDescribe', gitDescribe);
		Compiler.define('gitHead', gitHead);
	}

	public static function copyStaticFiles() {
		try {
			if (sys.FileSystem.exists("src/client/index.html")) {
				if (!sys.FileSystem.exists("build/bin/"))
					FileSystem.createDirectory("build/bin/");
				sys.io.File.copy("src/client/index.html", "build/bin/index.html");
				trace("'index.html' successfully copied to 'build/bin/'");
			}
		}
	}
	#end
}
