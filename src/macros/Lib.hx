package macros;

import haxe.macro.Context;
import haxe.macro.Expr;

class Lib {
	public static function addLibFields():Array<Field> {
		var fields:Array<Field> = Context.getBuildFields();

		fields.push({
			name: "main",
			access: [Access.APublic, Access.AStatic],
			kind: FieldType.FFun({
				args: [],
				ret: null,
				expr: macro Sys.println('Lib are not executable.')
			}),
			pos: Context.currentPos(),
		});

		fields.push({
			name: "GIT_DESCRIBE",
			access: [Access.APublic],
			kind: FieldType.FProp('default', 'never', macro:String, macro $v{Context.definedValue('gitDescribe')}),
			pos: Context.currentPos(),
			meta: [
				{name: ':keep', pos: Context.currentPos()},
				{name: ':readOnly', pos: Context.currentPos()}
			]
		});

		fields.push({
			name: "GIT_HEAD",
			access: [Access.APublic],
			kind: FieldType.FProp('default', 'never', macro:String, macro $v{Context.definedValue('gitHead')}),
			pos: Context.currentPos(),
			meta: [
				{name: ':keep', pos: Context.currentPos()},
				{name: ':readOnly', pos: Context.currentPos()}
			]
		});

		return fields;
	}
}
