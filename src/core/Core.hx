package core;

import cs.system.reflection.Assembly;

class Core implements Lib {
	private var modules = new Map();

	public function start() {
		trace('Core version: $GIT_HEAD($GIT_DESCRIBE)');
		for (file in sys.FileSystem.readDirectory('.')) {
			if (sys.FileSystem.isDirectory(file) || !StringTools.endsWith(file, '.mod'))
				continue;
			var module = null;
			var name = '';
			try {
				var assembly = Assembly.LoadFrom(file);
				name = assembly.GetName().Name;
				module = assembly.CreateInstance('${name.toLowerCase()}.$name');
			} catch (error:Dynamic) {
				trace(error);
			}
			if (module != null)
				modules.set(name, module);
		}
		trace(modules);
	}
}
