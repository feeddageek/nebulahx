package core;

@:autoBuild(macros.Lib.addLibFields())
interface Lib {
	public function start():Void;
	public var GIT_DESCRIBE(default, never):String;
	public var GIT_HEAD(default, never):String;
}
