package runtime;

import cs.system.reflection.Assembly;
import haxe.io.Path;

class Runtime {
	@:keep public static inline var GIT_DESCRIBE:String = macros.Macros.getDefined('gitDescribe');
	@:keep public static inline var GIT_HEAD:String = macros.Macros.getDefined('gitHead');

	public static function main() {
		Sys.setCwd((new Path(Sys.programPath())).dir);
		trace('Runtime version: $GIT_HEAD($GIT_DESCRIBE)');
		trace('Starting Core');
		try {
			var assembly = Assembly.Load('Core');
			var core = assembly.CreateInstance('core.Core');
			core.start();
			trace('Core started');
		} catch (error:Dynamic) {
			trace('Core could not be started');
			trace(error);
		}
	}
}
